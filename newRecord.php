<html>

<head>
    <!-- Link your php/css file -->
    <link rel="stylesheet" href="style.css" media="screen">
    <title>Create Call Log</title>
</head>

<body>

    <?php 
    
        //------------------------------------------------ Would Implement properly if had more time ------------------------------------------------//
        // Reset the error variables
        // $dateErr = $itpersonErr = $usernameErr = $subjectErr = $detailsErr = $hoursErr = $minutesErr ="";
        // Do some checks when sending data to server  
        // if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //     // If important fields are empty, notify the user where applicable
        //     if (empty($_POST["Date"])) { 
        //         $dateErr = "Date is required";
        //     }

        //     if (empty($_POST["ITPerson"])) { 
        //         $itpersonErr = "IT Person is required";
        //     }

        //     if (empty($_POST["UserName"])) { 
        //         $usernameErr = "User Name is required";
        //     }

        //     if (empty($_POST["Subject"])) { 
        //         $subjectErr = "Subject is required";    
        //     }

        //     if (empty($_POST["Details"])) { 
        //         $detailsErr = "Details is required";
        //     }

        //     if (empty($_POST["Total_Hours"])) { 
        //         $hoursErr = "Total Hours is required";
        //     }

        //     if (empty($_POST["Total_Minutes"])) { 
        //         $minutesErr = "Total Minutes is required";
        //     }
        // }
        //------------------------------------------------------------------------------------------------------------------------------------------------//
    ?>

    <form method="post" action="updatedb.php">
        <h1>Please enter your call details</h1>
        <table>
            <tr>
                <td>
                    <label>Date</label>
                </td>
                <td>
                    <input type="date" name="Date" value="<?php echo date('Y-m-d');?>">
                </td>
                <td>
                    <span class="error"><?php echo $dateErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>IT Person:</label>
                </td>
                <td>
                    <input type="text" name="ITPerson" maxlength="32" />
                </td>
                <td>
                    <span class="error"><?php echo $itpersonErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>User Name:</label>
                </td>
                <td>
                    <input type="text" name="UserName" maxlength="32" />
                </td>
                <td>
                    <span class="error"><?php echo $usernameErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Subject:</label>
                </td>
                <td>
                    <input type="text" name="Subject" maxlength="64" />
                </td>
                <td>
                    <span class="error"><?php echo $subjectErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Details:</label>
                </td>
                <td>
                    <textarea id="Details" name="Details" rows="6" cols="60"></textarea>
                </td>
                <td>
                    <span class="error"><?php echo $detailsErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Total Hours:</label>
                </td>
                <td>
                    <input type="number" name="Total_Hours" />
                </td>
                <td>
                    <span class="error"><?php echo $hoursErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Total Minutes:</label>
                </td>
                <td>
                    <input type="number" name="Total_Minutes" />
                </td>
                <td>
                    <span class="error"><?php echo $minutesErr;?></span>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Status</label>
                </td>
                <td>
                    <select id="Status" name="Status">
                        <option value="New">New</option>
                        <option value="In Progress">In Progress</option>
                        <option value="Completed">Completed</option>
                    </select>
                </td>
            </tr>
        </table>
        <label>
            <input type="submit"/>
            <button onclick="window.location.href='index.php'">Back</button>
        </label>
    </form>

</html>