<?php
    
    // Assign to variables for readability
    $date = $_POST['Date'];
    $itPerson = $_POST['ITPerson'];
    $userName = $_POST['UserName'];
    $subject = $_POST['Subject'];
    $details = $_POST['Details'];
    $totalHours = $_POST['Total_Hours'];
    $totalMinutes = $_POST['Total_Minutes'];
    $status = $_POST['Status'];
    
    // URL to route
    $actual_link = "http://$_SERVER[HTTP_HOST]/index.php";

    // For this excercise just hard code the DB info
    $servername = "127.0.0.1";
    $username = "root";
    $password = "root";


    // Attempt to connect to the DB (would separate this into a DB class if more time permitted)
    try {
        $conn = new PDO("mysql:host=$servername;dbname=agedcaredb", $username, $password);
        // set the PDO error mode to exception
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        // Insert statement for sql
        $sql = "INSERT INTO call_logs (Date, ITPerson, UserName, Subject, Details, Total_Hours, Total_Minutes, Status) VALUES (?,?,?,?,?,?,?,?)";

        // Prep the insert and add the data literals when executing
        $stmt = $conn->prepare($sql);

        $stmt->execute([$date, $itPerson, $userName, $subject, $details, $totalHours, $totalMinutes, $status]);

    } catch(PDOException $e) {
        echo "Connection failed: " . $e->getMessage();
    }

    // Close the connection to the DB
    $conn = null;

    // Route back to the index
    header("Location: $actual_link"); 
    exit();

?>