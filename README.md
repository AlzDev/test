# README #

Created in PHP 7.3.9.
Served Via 
php -S localhost:8000

landing page: http://localhost:8000/index.php

Please note that due to the time restriction, I wasn't able to make the code as tidy as I would have liked - nor have I completed all the requested tasks.
I would have split the application into more classes, and made the file structure a lot more tidyer.

Hopefully this gives you a small insight on my coding practices.


# Database Setup: #
Please set up with the following: (or change in the code if necessary) 
$servername = "127.0.0.1";
$username = "root";
$password = "root";

# Schema: #

CREATE DATABASE `agedcaredb` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

# Table: #

CREATE TABLE `call_logs` (
  `Callid` int NOT NULL AUTO_INCREMENT,
  `Date` date NOT NULL,
  `ITPerson` varchar(32) NOT NULL,
  `UserName` varchar(32) NOT NULL,
  `Subject` varchar(64) NOT NULL,
  `Details` longtext NOT NULL,
  `Total_Hours` int NOT NULL,
  `Total_Minutes` int NOT NULL,
  `Status` varchar(15) NOT NULL,
  PRIMARY KEY (`Callid`),
  UNIQUE KEY `Callid_UNIQUE` (`Callid`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
