<html>

<head>
    <!-- Link your php/css file -->
    <link rel="stylesheet" href="style.css" media="screen">
    <title>Search Call Logs</title>
</head>

<body>
    <?php
        // Logically iterate through the data and print each as a table row.
        class TableRows extends RecursiveIteratorIterator {
            function __construct($it) {
            parent::__construct($it, self::LEAVES_ONLY);
            }
        
            function current() {
            return "<td style='width:150px;border:1px solid black;'>" . parent::current(). "</td>";
            }
        
            function beginChildren() {
            echo "<tr>";
            }
        
            function endChildren() {
            echo "</tr>" . "\n";
            }
        }

        // For this excercise just hard code the DB info (would separate this into a DB class if more time permitted)
        $servername = "127.0.0.1";
        $username = "root";
        $password = "root";
        
        try {
            $conn = new PDO("mysql:host=$servername;dbname=agedcaredb", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // Insert statement for sql
            $sql = "SELECT * FROM call_logs ORDER BY Callid ASC";

            // Prep the insert and execute
            $stmt = $conn->prepare($sql);
            $stmt->execute();

            $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

        } catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
        }
        // Close the connection to the DB
        $conn = null;

            
        include("searchdb.php");

        function createQueryArray(){
            $query = array(
                "callid" => $_POST['CallId'],
                "username" => $_POST['UserName'],
                "date" => $_POST['Date']
            );

            $db = new db();
            return $db->selectWhere($query);

        }

        function createTable($stmt){
            foreach(new TableRows(new RecursiveArrayIterator($stmt->fetchAll())) as $k=>$v) {
                echo $v;
            }
        }
    ?>

    <form id="searchForm" method="post" >
        <label>Call ID</label>
        <input type="number" name="CallId"> </input>
        <label>User Name</label>
        <input type="text" name="UserName"> </input>
        <label>Date</label>
        <input type="date" name="Date"> </input>
        <br><br>
        <input type="submit" name="submit" />
        <br>
    </form>

    <!-- Build the top of the table -->
    <table style='border: solid 1px black;'>
        <tr>
            <th>CallId</th>
            <th>Date</th>
            <th>IT Person</th>
            <th>User Name</th>
            <th>Subject</th>
            <th>Details</th>
            <th>Total Hours</th>
            <th>Total Minutes</th>
            <th>Status</th>
        </tr>

        <!-- Create the rows for the table here -->
        <?php
            // When first routing in to the page, show all data
            if(!isset($_POST['submit'])){
                createTable($stmt);
            }
            
            // When the user clicks submit, only show that data
            if(isset($_POST['submit']))
            {
                $stmt = createQueryArray();
                $stmt->execute();
                $result = $stmt->setFetchMode(PDO::FETCH_ASSOC);

                createTable($stmt);
            }
        ?>
    </table>
    <br>
    <button onclick="window.location.href='index.php'">Back</button>
</body>

<?php


?>