<?php
    class db{
        
        // selectWhere();
        public function selectWhere($query){

            $query = array(
                "callid" => $_POST['CallId'],
                "username" => $_POST['UserName'],
                "date" => $_POST['Date']
            );
            
            $servername = "127.0.0.1";
            $username = "root";
            $password = "root";

            $conn = new PDO("mysql:host=$servername;dbname=agedcaredb", $username, $password);
            // set the PDO error mode to exception
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            // Compile the where query for the select statement depending on the paramaters passed from the search fields
            if($query['callid'] != ""){
                $whereQuery = "Callid = " . $query['callid'];
            }
            
            if($query['username'] != "") {
                $userConcat = "UserName = \"" . $query['username'] . "\"";
                if(isset($whereQuery)){
                    $whereQuery = $whereQuery . " AND " . $userConcat;
                } else{
                    $whereQuery = $userConcat;
                }
            }

            if($query['date'] != "") {
                $dateConcat = "Date = \"" . $query['date'] . "\"";
                if(isset($whereQuery)){
                    $whereQuery = $whereQuery . " AND " . $dateConcat;
                } else{
                    $whereQuery = $dateConcat;
                }
            }

            try {

                // Insert statement for sql
                $sql = "SELECT * FROM call_logs WHERE " . $whereQuery . " ORDER BY Callid ASC";
                
                // Prep the insert and execute
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                
                } catch(PDOException $e) {
                    echo "Error: " . $e->getMessage();
                }

                
                // Close the connection to the DB
                $conn = null;

                return $stmt;
        }
    }

?>